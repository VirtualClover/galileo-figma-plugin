import { endLoading, setLoading } from './loading.func';
import { populateDropdowns } from './populateDropdowns.func';

/**
 * Hides the child palette menu and shows the parent styleguide
 */
export function goToParentTab() {
  var activeClass = 'active';
  document.getElementById('child-menu').classList.remove(activeClass);
  document.getElementById('parent-menu').classList.add(activeClass);
  document.getElementById('child-tab').classList.remove(activeClass);
  document.getElementById('parent-tab').classList.add(activeClass);
}

/**
 * Hides the parent styleguide menu and shows the child palette
 * @param styleguide The styleguide generated from the galileo API
 */
export async function goToChildTab(styleguide: Styleguide) {
  setLoading();
  var activeClass = 'active';
  document.getElementById('parent-menu').classList.remove(activeClass);
  document.getElementById('child-menu').classList.add(activeClass);
  document.getElementById('parent-tab').classList.remove(activeClass);
  document.getElementById('child-tab').classList.add(activeClass);
  await populateDropdowns(styleguide);
  endLoading();
}
