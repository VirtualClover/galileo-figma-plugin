/**
 * Sets inputs like buttons and dropdowns to disabled
 */
export function setLoading() {
  const buttons = document.getElementsByTagName('button');
  const selects = document.getElementsByTagName('select');
  for (let i = 0; i < buttons.length; i++) {
    buttons[i].disabled = true;
  }
  for (let i = 0; i < selects.length; i++) {
    selects[i].disabled = true;
  }
}

/**
 * Reenables inputs like buttons and dropdowns
 */
export function endLoading() {
  const buttons = document.getElementsByTagName('button');
  const selects = document.getElementsByTagName('select');
  for (let i = 0; i < buttons.length; i++) {
    buttons[i].removeAttribute('disabled');
  }
  for (let i = 0; i < selects.length; i++) {
    selects[i].removeAttribute('disabled');
  }
}
