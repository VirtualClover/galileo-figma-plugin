//Populate the dropdowns

/**
 * Populates the dropdowns <select> of the child palette menu with <option>
 * tag containing the general styleguide's shades, then defaluts the dropdowns to the "Choose shade" value
 * @param styleguide
 */
export async function populateDropdowns(styleguide: Styleguide) {
  //console.log(styleguide);
  try {
    const dropdowns = <HTMLCollectionOf<HTMLSelectElement>>(
      document.getElementsByClassName('shadePicker')
    );

    const variantDropdown = <HTMLSelectElement>(
      document.getElementById('themeVariant')
    );
    const themeVariant = variantDropdown.value;

    const colorList = styleguide.colors;
    for (let i = 0; i < dropdowns.length; i++) {
      let dropdown = dropdowns[i];
      dropdown.length = 0;

      let defaultOption = document.createElement('option');
      defaultOption.text = '----';

      dropdown.add(defaultOption);
      dropdown.selectedIndex = 0;

      for (var key in colorList) {
        if (!colorList.hasOwnProperty(key)) continue;
        var color = colorList[key];
        let parentKey = key;
        for (var key in color) {
          let option = document.createElement('option');
          option.text = `${parentKey} ${key}`;
          option.dataset.color = parentKey;
          option.dataset.shade = key;
          option.value = `${dropdown.id}/${parentKey}/${key}`;
          dropdown.add(option);
          parent.postMessage(
            {
              pluginMessage: {
                type: 'gatherChildStyles',
                shadeName: `${parentKey}/${key}`,
                dropdownId: dropdown.id,
                themeVariant,
              },
            },
            '*'
          );
        }
      }
    }
  } catch (e) {
    console.error(e);
  }
}

/**
 * If it exists, stablishes the dropdowns with an existing shade in the document,
 * for example, if the style solid/primary with the shade echo/300 already exsits the dropdown
 * with the id"primary" auto selects the option echo/300
 * @param existingStyles an array empty that will contain existing styles
 * @param data the existing styles of figma
 */
export function updateDropdowns(existingStyles, data) {
  if (data.type == 'existingStyle') {
    existingStyles.push(data);
    //console.log(data);
    for (let f = 0; f < existingStyles.length; f++) {
      let dropdown = <HTMLInputElement>document.getElementById(data.name);
      dropdown.value = `${data.name}/${data.description}`;
      //console.log(`${data.name}/${data.description}`);
    }
  }
}
