import { endLoading, setLoading } from '.';

/**
 * Generates the whole styleguide
 * @param styleguide
 */
export const parentStyleguide = async (styleguide: Styleguide) => {
  setLoading();
  await parent.postMessage(
    { pluginMessage: { type: 'createStyles', styleguide } },
    '*'
  );
  endLoading();
};
