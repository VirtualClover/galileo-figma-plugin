import { endLoading, setLoading } from '.';

/**
 * Generates a child styleguide
 * @param styleguide
 */
export const childStyleguide = async (styleguide: Styleguide) => {
  setLoading();
  try {
    const dropdowns = <HTMLCollectionOf<HTMLSelectElement>>(
      document.getElementsByClassName('shadePicker')
    );
    const variantDropdown = <HTMLSelectElement>(
      document.getElementById('themeVariant')
    );
    const themeVariant = variantDropdown.value;

    setLoading();
    const colorList = styleguide.colors;
    let palette = [];
    //Get the values
    for (let i = 0; i < dropdowns.length; i++) {
      let dropdown = dropdowns[i];
      let selectedIndex = dropdown[dropdown.selectedIndex];
      let color = selectedIndex.dataset.color;
      let shade = selectedIndex.dataset.shade;
      let values = colorList[color][shade];
      let key = dropdown.getAttribute('id');
      let object = { key, color, shade, values };
      palette.push(object);
    }
    await parent.postMessage(
      { pluginMessage: { type: 'createChildStyles', palette, themeVariant } },
      '*'
    );
  } catch (e) {
    console.error(e);
  }
  endLoading();
};
