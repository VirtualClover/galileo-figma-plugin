/**
 * The color interface contains the information of a color shade, like Echo100
 */
interface ColorShade {
  hex: string;
  rgb: string;
  hsl: string;
  recommended_foreground: string;
  description: string;
}

/**
 * The JSON recieved from the Galielo API
 */
interface Styleguide {
  colors: Array<any>;
  textStyles: Array<any>;
  options: Array<any>;
}

/**
 * RGB values
 */
interface rgb {
  r:number,
  g:number,
  b: number
}