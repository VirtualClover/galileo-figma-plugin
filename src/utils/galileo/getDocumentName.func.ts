/**
 * Gets the name of the current figma file
 */
export function getDocumentName() {
  const FILE = figma.root;
  return FILE.name;
}
