/**
 * Creates a text style, if the existing style is specified, it edits it instead of creating one
 * @param fontName
 * @param name
 * @param fontSize
 * @param existingStyle
 * @returns
 */
export async function createTextStyle(
  fontName: FontName,
  name: string,
  fontSize: number,
  lineHeight: number,
  textDecoration: string,
  description: string,
  existingStyle?: TextStyle
) {
  //Loads the font
  await figma.loadFontAsync(fontName);
  if (!existingStyle) {
    const textStyle = figma.createTextStyle();
    textStyle.name = name;
    return generate(
      textStyle,
      name,
      fontName,
      textDecoration,
      fontSize,
      lineHeight,
      description
    );
  } else {
    return generate(
      existingStyle,
      name,
      fontName,
      textDecoration,
      fontSize,
      lineHeight,
      description
    );
  }
}

/**
 * Separated in a new function because it's the same process once a style is created or gather.
 * @param textStyle
 * @param name
 * @param fontName
 * @param textDecoration
 * @param fontSize
 * @param lineHeight
 * @param description
 * @returns
 */
function generate(
  textStyle: TextStyle,
  name: string,
  fontName: FontName,
  textDecoration: string,
  fontSize: number,
  lineHeight: number,
  description: string
) {
  textStyle.name = name;
  textStyle.fontName = fontName;
  textStyle.textDecoration = <TextDecoration>textDecoration.toUpperCase();
  textStyle.fontSize = fontSize;
  textStyle.lineHeight = { value: lineHeight, unit: 'PIXELS' };
  textStyle.description =  description ? description : '';
  return textStyle;
}
