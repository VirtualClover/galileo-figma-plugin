/**
 * Gets a text style by an attribute
 * @param array
 * @param attr
 * @param value
 * @returns
 */
export function getTextStyleByAttr(array, attr: string, value: string | number) {
  //console.log(array);
  for (var i = 0; i < array.length; i += 1) {
    if (array[i][attr] === value) {
      return i;
    }
  }
  return -1;
}
