/**
 * Returns an existing style in the figma library if it exists
 * @param array The current library of the file using figma.getLocalPaintStyles();
 * @param attr
 * @param value
 * @returns The index of the color within the passed array
 */
export function getSolidStyleByAttr(
  array: PaintStyle[],
  attr: string,
  value: string,
  secondAttr?: string,
  secondValue?: string
) {
  for (var i = 0; i < array.length; i += 1) {
    if (
      array[i][attr] === value &&
      (secondValue ? array[i][secondAttr] === secondValue : true)
    ) {
      return i;
    }
  }
  return -1;
}
