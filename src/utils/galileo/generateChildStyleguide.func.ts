import { getDocumentName } from './getDocumentName.func';
import { createSolidStyle } from './styles/createSolidStyle.func';
import { getSolidStyleByAttr } from './styles/getSolidStyleByAttr.func';

export function generateChildStyleguide(themeVariant, palette) {
  //console.log(palette);
  var colorList = palette;
  var paintStyles = figma.getLocalPaintStyles();
  var i = 0,
    len = colorList.length;
  while (i < len) {
    const styleName = `${
      getDocumentName().split('-')[0]
    }-${themeVariant}/solid/${colorList[i].key}`;
    const description = `${colorList[i].color}/${colorList[i].shade}`;
    const styleIndex = getSolidStyleByAttr(paintStyles, 'name', styleName);
    createSolidStyle(
      colorList[i].values,
      styleName,
      description,
      paintStyles[styleIndex]
    );
    i++;
  }
}
