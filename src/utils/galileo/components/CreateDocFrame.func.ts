import { getTextStyleByAttr } from '../styles/getTextStyleByAttr.func';
import { createTextLayer } from './CreateTextLayer.func';

export async function createDocFrame(title: string, content: SceneNode[]) {
  const frame = figma.createFrame();
  frame.name = title;
  const parentGroup = <FrameNode>(
    figma.currentPage.findOne(
      (node) => node.type === 'FRAME' && node.name === title
    )
  ); //We nesure to specify that this method should always return a frame node;
  parentGroup.verticalPadding = 150;
  parentGroup.horizontalPadding = 150;
  parentGroup.layoutMode = 'VERTICAL';
  parentGroup.counterAxisSizingMode = 'AUTO';
  parentGroup.primaryAxisSizingMode = 'AUTO';
  parentGroup.primaryAxisAlignItems = 'MAX';
  parentGroup.itemSpacing = 150;

  await figma.loadFontAsync({ family: 'Bitso', style: 'Bold' }).then(() => {
    //Loads the font
    const headingIndex = getTextStyleByAttr(
      figma.getLocalTextStyles(),
      'name',
      'heading/desktop'
    );
    const heading = figma.getLocalTextStyles()[headingIndex];
    const layer = createTextLayer(title, heading.id);
    parentGroup.appendChild(layer);
  });

  for (let i = 0; i < content.length; i++) {
    parentGroup.appendChild(content[i]);
  }

  await figma.loadFontAsync({ family: 'Bitso', style: 'Bold' }).then(() => {
    //Loads the font
    const footerIndex = getTextStyleByAttr(
      figma.getLocalTextStyles(),
      'name',
      'paragraph/desktop'
    );
    const footer = figma.getLocalTextStyles()[footerIndex];
    const layer = createTextLayer(
      'This frame was created automatically by galileo.',
      footer.id
    );
    parentGroup.appendChild(layer);
  });
}
