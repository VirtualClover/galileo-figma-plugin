/**
 * Creates a text layer, always remember to put this method in the .then of a figma.asycnLoadFont
 * @param string
 * @param textStyleId
 * @returns the layer
 */
export function createTextLayer(
  string: string,
  textStyleId: string,
  textAlign?: TextNode['textAlignHorizontal']
) {
  const layer = figma.createText();
  layer.textStyleId = textStyleId;
  layer.characters = string;
  layer.layoutAlign = 'INHERIT';
  layer.textAlignHorizontal = textAlign ? textAlign : 'LEFT';
  return layer;
}
