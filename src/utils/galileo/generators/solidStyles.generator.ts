import { createSolidStyle } from '../styles/createSolidStyle.func';
import { getSolidStyleByAttr } from '../styles/getSolidStyleByAttr.func';

/**
 * Generates all solid styles given an array of it.
 * @param solidStyles
 * @param existingSolidStyles
 */
export function solidStylesGenerator(solidStyles, existingSolidStyles) {
  for (var key in solidStyles) {
    if (!solidStyles.hasOwnProperty(key)) continue;
    var color = solidStyles[key];
    var parentKey = key;

    for (var key in color) {
      if (!color.hasOwnProperty(key)) continue;

      const styleName = `solid/${parentKey}/${key}`;
      const styleIndex = getSolidStyleByAttr(
        existingSolidStyles,
        'name',
        styleName
      );

      createSolidStyle(
        color[key],
        styleName,
        color[key].description,
        existingSolidStyles[styleIndex]
      );
    }
  }
}
