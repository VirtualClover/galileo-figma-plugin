import { createTextStyle } from "../styles/createTextStyle.func";
import { getTextStyleByAttr } from "../styles/getTextStyleByAttr.func";

/**
 * Generates all of the text styles, given an array.
 * @param textStyles 
 * @param existingTextStyles 
 */
export function textStylesGenerator(textStyles, existingTextStyles){
    for (var key in textStyles) {
        if (!textStyles.hasOwnProperty(key)) continue;
        var textStyle = textStyles[key];
        var parentKey = key;
    
        for (var key in textStyle) {
          if (!textStyle.hasOwnProperty(key)) continue;
    
          const styleName = `${parentKey}/${key}`;
          const styleIndex = getTextStyleByAttr(
            existingTextStyles,
            'name',
            styleName
          );
    
          createTextStyle(
            { family: textStyle[key].font, style: textStyle[key].weight },
            styleName,
            parseInt(textStyle[key].size),
            parseInt(textStyle[key].lineHeight),
            textStyle[key].decoration,
            textStyle[key].description,
            existingTextStyles[styleIndex]
          );
        }
      }
};
