import { createTextLayer } from './components';
import { createSolidStyle } from './styles/createSolidStyle.func';
import { getSolidStyleByAttr } from './styles/getSolidStyleByAttr.func';
import { createTextStyle } from './styles/createTextStyle.func';
import { getTextStyleByAttr } from './styles/getTextStyleByAttr.func';
import { textStylesGenerator } from './generators/textStyles.generator';
import { solidStylesGenerator } from './generators/solidStyles.generator';
import { createDocFrame } from './components/CreateDocFrame.func';

export async function generateParentStyleguide(styleguide: Styleguide) {
  const solidTitle = 'Color palette';

  const existingFrame = figma.currentPage.findOne(
    (node) => node.type === 'FRAME' && node.name == solidTitle
  );
  if (existingFrame) {
    existingFrame.remove();
  }

  const solidStyles = styleguide.colors;
  const textStyles = styleguide.textStyles;

  const existingSolidStyles = figma.getLocalPaintStyles();
  const existingTextStyles = figma.getLocalTextStyles();

  await solidStylesGenerator(solidStyles, existingSolidStyles);
  await textStylesGenerator(textStyles, existingTextStyles);

  const content = [];

  const paletteGroup = figma.createFrame();
  paletteGroup.layoutMode = 'HORIZONTAL';
  paletteGroup.counterAxisSizingMode = 'AUTO';
  paletteGroup.primaryAxisSizingMode = 'AUTO';
  paletteGroup.counterAxisAlignItems = 'MAX';
  paletteGroup.itemSpacing = 0;

  for (var key in solidStyles) {
    if (!solidStyles.hasOwnProperty(key)) continue;
    var color = solidStyles[key];
    var parentKey = key;
    const row = figma.createFrame();
    row.name = parentKey;
    row.layoutMode = 'VERTICAL';
    row.primaryAxisSizingMode = 'AUTO';
    row.counterAxisSizingMode = 'AUTO';
    row.itemSpacing = 0;

    for (var key in color) {
      if (!color.hasOwnProperty(key)) continue;

      const dimensions = 160;
      const shadeGroup = figma.createFrame();
      shadeGroup.name = key;
      shadeGroup.resize(dimensions, dimensions);

      const rectangle = figma.createRectangle();
      const mappedIndex = getSolidStyleByAttr(
        figma.getLocalPaintStyles(),
        'name',
        `solid/${parentKey}/${key}`
      );
      rectangle.name = key;
      rectangle.fillStyleId = figma.getLocalPaintStyles()[mappedIndex].id;
      rectangle.resize(dimensions, dimensions);
      shadeGroup.appendChild(rectangle);

      const shadeDataGroup = figma.createFrame();
      shadeDataGroup.layoutMode = 'VERTICAL';
      shadeDataGroup.fills = [];
      //shadeDataGroup.name = 'data';

      await figma.loadFontAsync({ family: 'Bitso', style: 'Bold' }).then(() => {
        //Loads the font
        const regularIndex = getTextStyleByAttr(
          figma.getLocalTextStyles(),
          'name',
          'paragraph/desktop'
        );
        const regular = figma.getLocalTextStyles()[regularIndex];
        const name = `${parentKey}${key}`;
        const clearPaint: Paint[] = [
          { type: 'SOLID', color: { r: 1, g: 1, b: 1 } },
        ];
        const darkPaint: Paint[] = [
          { type: 'SOLID', color: { r: 0, g: 0, b: 0 } },
        ];
        const contrast = solidStyles[parentKey][key].recommended_foreground;
        const shadeName = createTextLayer(name, regular.id, 'CENTER');
        const hex = createTextLayer(
          solidStyles[parentKey][key].hex,
          regular.id,
          'CENTER'
        );
        shadeName.setRangeFills(
          0,
          name.length,
          contrast == 'light' ? clearPaint : darkPaint
        );
        hex.setRangeFills(
          0,
          solidStyles[parentKey][key].hex.length,
          contrast == 'light' ? clearPaint : darkPaint
        );
        shadeDataGroup.appendChild(shadeName);
        shadeDataGroup.appendChild(hex);
        shadeGroup.appendChild(shadeDataGroup);
      });

      shadeDataGroup.primaryAxisSizingMode = 'AUTO';
      shadeDataGroup.counterAxisSizingMode = 'AUTO';
      shadeDataGroup.primaryAxisAlignItems = 'CENTER';
      shadeDataGroup.counterAxisAlignItems = 'CENTER';
      shadeDataGroup.x = dimensions / 2 - shadeDataGroup.width / 2;
      shadeDataGroup.y = dimensions / 2 - shadeDataGroup.height / 2;

      row.appendChild(shadeGroup);
    }
    paletteGroup.appendChild(row);
  }

  content.push(paletteGroup);
  createDocFrame(solidTitle, content);
}
