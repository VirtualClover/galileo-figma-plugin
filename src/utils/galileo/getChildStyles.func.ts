import { getDocumentName } from './getDocumentName.func';
import { getSolidStyleByAttr } from './styles/getSolidStyleByAttr.func';

/**
 * Gathers all the color styles of a child palette, unlike getColorStyleByAttr,
 * this method formats the styles a specific way.
 * @param shadeName
 */
export function getChildStyles(
  themeVariant: string,
  id: string,
  shadeName: string
) {
  const localStyleguide = figma.getLocalPaintStyles();
  const existingStyleIndex = getSolidStyleByAttr(
    localStyleguide,
    'description',
    shadeName,
    'name',
    `${getDocumentName().split('-')[0]}-${themeVariant}/solid/${id}`
  );
  const exsitingStyle = localStyleguide[existingStyleIndex];
  figma.ui.postMessage({
    type: 'existingStyle',
    description: exsitingStyle.description,
    name: exsitingStyle.name.split('solid/')[1],
  });
}
