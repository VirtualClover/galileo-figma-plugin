import { setLoading, endLoading } from '../ui';
/**
 * Fetches the styleguide from the db
 * */
export async function fetchStyleguide(): Promise<Styleguide> {
  setLoading();
  document.getElementById('preloader').removeAttribute('class');
  let styleguide: Styleguide;
  try {
    await fetch('https://galileo-api-bqjgycnf5q-uc.a.run.app/styleguide', {
      method: 'GET',
    })
      .then((response) => response.json())
      .then((data) => {
        styleguide = data.data;
        //console.log(styleguide);
      });
    endLoading();
    document.getElementById('preloader').setAttribute('class', 'finished');
    return styleguide;
  } catch (e) {
    console.log(e);
  }
}
