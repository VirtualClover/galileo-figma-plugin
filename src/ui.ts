import './ui.css';
import { endLoading, goToChildTab, goToParentTab, setLoading } from './utils/ui';
import { fetchStyleguide } from './utils/services';
import { childStyleguide, parentStyleguide } from './utils/ui';
import {
  populateDropdowns,
  updateDropdowns,
} from './utils/ui/populateDropdowns.func';

window.onload = function () {
  //Get the styleguide from endpoint
  let styleguide: Styleguide;
  let existingStyles = [];
  (async () => {
    styleguide = await fetchStyleguide();
  })();

  //Generate the whole styleguide
  document.getElementById('generate').onclick = async () => {
    parentStyleguide(styleguide);
  };
  //Tab switching
  document.getElementById('parent-tab').onclick = () => {
    goToParentTab();
  };
  document.getElementById('child-tab').onclick = () => {
    goToChildTab(styleguide);
  };
  //Generate the child styleguide
  document.getElementById('generate_child_styleguide').onclick = async () => {
    childStyleguide(styleguide);
  };

  document.getElementById('themeVariant').onchange = async () => {
    setLoading();
    await populateDropdowns(styleguide);
    endLoading();
  };

  //message listener
  onmessage = (event) => {
    const data = event.data.pluginMessage;
    if (data.type == 'existingStyle') {
      updateDropdowns(existingStyles, data);
    }
  };
};
