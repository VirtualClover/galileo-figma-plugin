// You can access browser APIs in the <script> tag inside "ui.html" which has a
// full browser environment (see documentation).

import {
  generateParentStyleguide,
  generateChildStyleguide,
  getChildStyles,
} from './utils/galileo';

// This shows the HTML page in "ui.html".
figma.showUI(__html__, { width: 350, height: 400 });

// Calls to "parent.postMessage" from within the HTML page will trigger this
// callback. The callback will be passed the "pluginMessage" property of the
// posted message.
figma.ui.onmessage = async (msg) => {
  //Initialize fonts
  //console.log(msg);
  // One way of distinguishing between different types of messages sent from
  // your HTML page is to use an object with a "type" property like this.
  switch (msg.type) {
    case 'gatherChildStyles':
      getChildStyles(msg.themeVariant, msg.dropdownId, msg.shadeName);
      break;

    case 'createStyles':
      generateParentStyleguide(msg.styleguide);
      break;

    case 'createChildStyles':
      generateChildStyleguide(msg.themeVariant, msg.palette);
      break;

    case 'deleteExtraStyles':
      break;
    default:
      break;
  }
};
